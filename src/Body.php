<?php

namespace HugoGitlabWebhook;

class Body
{
  private $projectName;
  private $webUrl;
  private $branch;
  private $user_name;
  private $user_email;

  public function __construct(\stdClass $body) {
    $this->setProjectName($body->project->name);
    $this->setWebUrl($body->project->web_url);
    $this->setBranch($body->project->default_branch);
    $this->setUserName($body->user_username);
    $this->setUserEmail($body->user_email);
  }

  private function setProjectName($name)
  {
    $this->projectName = $name;
  }

  public function getProjectName()
  {
    return $this->projectName;
  }

  private function setWebUrl($url)
  {
    $this->webUrl = $url;
  }

  public function getWebUrl()
  {
    return $this->webUrl;
  }

  private function setBranch($branch)
  {
    $this->branch = $branch;
  }

  public function getBranch()
  {
    return $this->branch;
  }

  private function setUserName($username)
  {
    $this->user_name = $username;
  }

  public function getUserName()
  {
    return $this->user_name;
  }

  private function setUserEmail($email)
  {
    $this->user_email = $email;
  }

  public function getUserEmail()
  {
    return $this->user_email;
  }
}
