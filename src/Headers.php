<?php

namespace HugoGitlabWebhook;

class Headers
{
  private $gitlabEvent;
  private $gitlabToken;
  private $contentType;
  private $connection;

  public function __construct($headers, $tokenProject)
  {
    $this->setGitlabEvent($headers['X-Gitlab-Event'][0]);
    $this->setGitlabToken($headers['X-Gitlab-Token'][0], $tokenProject);
    $this->setContentType($headers['Content-Type'][0]);
    $this->setConnection($headers['Connection'][0]);
  }

  public function setGitlabEvent($event)
  {
    if ($event === 'Push Hook') {
      $this->gitlabEvent = $event;
    } else {
      throw new \Exception("Bad Header Event");
    }
  }

  public function setGitlabToken($token, $tokenProject)
  {
    if ($token === sha1($tokenProject)) {
      $this->gitlabToken = $token;
    } else {
      throw new \Exception("Bad Header Token");
    }
  }

  public function setContentType($type) {
    if ($type === 'application/json') {
      $this->contentType = $type;
    } else {
      throw new \Exception("Bad Header Content-Type");
    }
  }

  public function setConnection($connection) {
    if ($connection === 'close') {
      $this->connection = $connection;
    } else {
      throw new \Exception("Bad Header Connection");
    }
  }
}
