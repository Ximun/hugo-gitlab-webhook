<?php

namespace HugoGitlabWebhook;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Response;
use HugoGitlabWebhook\Project;
use HugoGitlabWebhook\Body;
use HugoGitlabWebhook\RequestProject;
use HugoGitlabWebhook\Headers;
use GitWrapper\GitWrapper;
use function Http\Response\send;

/*
 * Reçoit la requête, stocke les paramètres et vérifie la requête
 *
 */
class HugoGitlabWebhook
{
  /*
   * The HTTP request
   */
  public $request;

  /*
   * The HTTP response
   */
  public $response;

  /*
   * Array of Headers from the HTTP Request
   */
  public $headers;

  /*
   * Object from Body Json HTTP Request
   */
  public $body;

  /*
   * Array of Projects from config.json
   */
  public $projects = [];

  /*
   * Project object based from the Gitlab Hugo Website project
   */
  public $requestProject;

  /*
   * Local Folder Website on server
   */
  public $localFolder;

  /*
   * Headers Required
   */
  private $headers_test = [
    'X-Gitlab-Event' => 'Push Hook',
    'Content-Type' => 'application/json',
    'X-Gitlab-Token' => 'token',
    'Connection' => 'close'
  ];


  /*
   * Construct objects based on the HTTP Request
   */
  public function __construct(ServerRequestInterface $request)
  {
    $this->request = $request;
    $this->body = new Body(json_decode((string)$this->request->getBody()));
    $this->project = new Project($this->selectProject());
    $this->headers = new Headers($request->getHeaders(), 'token');

    $this->gitWrapper = new GitWrapper();
    $this->response = new Response(200, [], 'good!');
    $this->returnResponse();

  }

  private function selectProject()
  {
    $projects = include('config.php');

    foreach ($projects as $project)
    {
      if ($project['name'] === $this->body->getProjectname()) {
        return $project;
        break;
      }
    }
  }

  /**
   * Send the response
   */
  private function returnResponse()
  {
    return $this->response;
  }

}
