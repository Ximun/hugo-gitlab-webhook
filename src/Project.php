<?php

namespace HugoGitlabWebhook;

class Project
{

  private $name;
  private $localFolder;
  private $token;
  private $repositoryUrl;

  public function __construct(Array $project)
  {
    $this->name = $project['name'];
    $this->repositoryUrl = $project['repository_url'];
    $this->token = $project['token'];
    $this->localFolder = $project['local_folder'];
  }

  public function setName($name)
  {
    $this->name = $name;
  }

  public function getName()
  {
    return $this->name;
  }

  public function setLocalFolder($localFolder)
  {
    $this->localFolder = $localFolder;
  }

  public function getLocalFolder()
  {
    return $this->localFolder;
  }

  public function setToken($token)
  {
    $this->token = $token;
  }

  public function getToken()
  {
    return $this->token;
  }

  public function setRespositoryUrl($repositoryUrl)
  {
    $this->repositoryUrl = $repositoryUrl;
  }

  public function getRespositoryUrl()
  {
    return $this->repositoryUrl;
  }

}
