<?php

namespace HugoGitlabWebhook;

use HugoGitlabWebhook\Project;

class RequestProject extends Project
{

  public function __construct($name, $repositoryUrl, $localFolder)
  {
    parent::__construct($name, $repositoryUrl, $localFolder);
  }
}
