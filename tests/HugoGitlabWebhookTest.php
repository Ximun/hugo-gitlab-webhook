<?php

use PHPUnit\Framework\TestCase;
use HugoGitlabWebhook\HugoGitlabWebhook;
use GuzzleHttp\Psr7\ServerRequest;

class HugoGitlabWebhookTest extends TestCase
{

  protected static $headers;
  protected static $body;

  public static function setUpBeforeClass(): void
  {
    self::$headers = [
      "X-Gitlab-Event" => "Push Hook",
      "X-Gitlab-Token" => "token",
      "Content-Type" => "application/json",
      "Connection" => "close",
      "Host" => "webhooks.gwendalavir.eu"
    ];

    self::$body = file_get_contents('body.json');
  }

  public function testPostRequestMethod()
  {
    $request = new ServerRequest('POST', '/', self::$headers, self::$body);
    $app = new HugoGitlabWebhook($request);
    $response = $app->response;
    $this->assertEquals(200, $response->getStatusCode());
  }

  public function testWrongRequestMethod()
  {
    $request = new ServerRequest('GET', '/', self::$headers, self::$body);
    $app = new HugoGitlabWebhook($request);
    $response = $app->response;
    $this->assertEquals(403, $response->getStatusCode());
  }

  public function testBodyNotJson()
  {
    $request = new ServerRequest('POST', '/', self::$headers, 'not JSON');
    $app = new HugoGitlabWebhook($request);
    $response = $app->response;
    $this->assertEquals(403, $response->getStatusCode());
  }

  public function testNoHeaders()
  {
    $request = new ServerRequest('POST', '/', [], self::$body);
    $app = new HugoGitlabWebhook($request);
    $response = $app->response;
    $this->assertEquals(403, $response->getStatusCode());
  }

  // How to test different config.json file?
}
